const { UserCredentials } = require('./usersClass.js'); 

// Creating a basic class type for perfoming some basic mathematical calculations
class MathsOperations extends UserCredentials {
    // Constructor
    constructor(_params) {
        super(_params) 
        // Get the params
        this.x1 = _params.x1;
        this.x2 = _params.x2;
        this.y1 = _params.y1;
        this.y2 = _params.y2;

        }
    

    // Creating a fuction for getting the gradient
    /* m = (y2 - y1)/ (x2 - x1) */
    calculateGradient = () => {
        // Execute this block of code if the "calculateGradient" function was called upon
        let gradient = (this.y2 - this.y1) / (this.x2 - this.x1);
        gradient = this.convert_to_x_dp(1, gradient)
        return gradient;

    }

    // Creating a function for converting the numbers in the specified decimal places 
    convert_to_x_dp = (decimal_places, numberValue) => {
        // Cleaning up the user's input (PS: Never trust the users input) 
        decimal_places = Number.parseInt(decimal_places); 
        numberValue = Number.parseFloat(numberValue); 

        // Execute this block of code below 
        let int_part = String(numberValue).split(".")[0]; 
        let floating_point = String(numberValue).split(".")[1]; 

        // Creating a list for holding the splitted number 
        let listNumbers = [int_part.concat(".")]; 

        // If the user types zero as a value for the decimal places 
        if ( decimal_places === 0 ) {
            // Execute the block of code below 
            return Number.parseInt(int_part); 
        }

        // If the parsed value for the decimal places is greater than the floating point value length, 
        // execute the block of code below 
        if ( decimal_places > floating_point.length ) {
            // Return the number value 
            return numberValue; 
        }

        // looping and converting the numbers to the specified decimal places 
        for ( let i = 0; i < floating_point.length; i++ ) {
            // Execute the block of code below if the 
            // decimal places equals the value for i 
            if ( i === decimal_places - 1 ) {
                listNumbers.push(floating_point[i]); 

                // Return the final approximated results 
                let results = Number.parseFloat(listNumbers.join("")); 
                return results; 
            }

            // Keep on adding the values for the floating point numbers into the 
            // listnumber arrays, until the value for the decimal places equal the value for i 
            listNumbers.push(floating_point[i]); 
        }
    }
}


// Exporting the class
module.exports.MathsOperations = MathsOperations;
