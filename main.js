// Importing the class 
const { UserCredentials } = require('./usersClass.js'); 
const { MathsOperations } = require('./maths.js'); 

// Creating an object about kelvin 
const kelvinDetails = {
    firstname: "Brute", 
    lastname: "Kelvin", 
    age: 24, 
    location: "London", 
    country: "UK", 
    latitude: "23.985",
    longitude: "-23.098"
}

// Creating an object for calculating the gradient 
const co_ordinates_data = {
    x1: 23.4, 
    x2: 0.987, 
    y1: 100.98, 
    y2: .987
}

// Creating an instance of the MathOperations 
const mathClass = new MathsOperations(co_ordinates_data); 
let gradient = mathClass.calculateGradient(); 
// results = mathClass.convert_to_x_dp(2, gradient)

console.log(gradient); 



// console.log(mathClass.calculateGradient().toFixed(3)); 

// // Creating an instance of the UserCredentials 
// const kevlinClass = new UserCredentials(kelvinDetails); 

// // Calling some functions inside the UserCredentials 
// kevlinClass.displayAgeAndLocation(); 