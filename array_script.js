/* Creating a function to get the value for the first row */
let row_one_data = ["FirstName", "LastName", "Age", "Sex", "Score"];
let row_array = {};

// Creating a function to get the input data from the input html tag
function getInputValue()
{
  // Selecting the input element and getting it's value
  let first_name = document.getElementById("row_1_col_1").value;
  let last_name = document.getElementById("row_1_col_2").value;
  let age = document.getElementById("row_1_col_3").value;
  let sex = document.getElementById("row_1_col_4").value;
  let score = document.getElementById("row_1_col_5").value;

  // Displaying the value
  alert(first_name + last_name + age + sex);

}

// Creating a function for extracting data from the row1 inside the table
function row_one()
{
  // Getting the number of data that exist in the first row
  const numOf_rowOne_elements = document.getElementsByClassName("row_1").length;

  // Calling the function for extraction of data/value from the first row
  getInputValue();

}
































// // looping through an array
// for ( var i = 0; i < cars.length; i++ )
// {
//   console.log(cars[i]);
// }

// Creating a variable to hold the cars
// var cars = [
//   "golf",
//   "polo",
//   "a3",
//   "mercedes"
// ];
//
// // Creating a variable to hold numbers
// var num = [25, 30, 1, 29, 32, 7, 5, 98];
//
// //
// var res = num.filter(function(element) {
//   console.log(element);
//   return element;
// });
//
// // Creating a function to convert all the strings in the
// // cars list into upper case
// var string_arr = ["a", "b", "c"];
//
// // using the mapping function
// var mapping = string_arr.map(function(element){
//
//   // returning the element by converting them into upper case
//   return element.toUpperCase();
// })
//
//
//
// console.log(num);
// console.log(res);
// console.log(mapping);
//
// // Using the map method
// var arrOne = [2, 3, 4, 5];
// var res = arrOne.map(function(arrValues){
//
//   // Performing operation on the array values
//   return arrValues * 10;
// })
//
// console.log(res);
// console.log(arrOne);












// // Creating a script to hold array
// var cars = ["bmw", "kia", "mercedes", "fiat"];
// console.log(cars[0]);
// console.log(cars);
//
// //
// var fruits = ["banana", "apple"];
// var arr2 = [1, 2, 3];
//
// var res = arr2.concat(fruits);
// console.log(fruits);
// console.log(res);
//
//
// // looking for a specific file in the list cars
// const search_result = cars.lastIndexOf("dog");
//
// /* If the search result returns -1, then it means that
// the value/string is not present in the list */
// console.log(search_result);
//
// // looping through an array
// for ( var i = 0; i < cars.length; i++ )
// {
//   console.log(cars[i]);
// }
