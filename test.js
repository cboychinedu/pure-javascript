// 
let number = 23.985323; 


let convert_to_x_dp = (decimal_places, numberValue) => {
    // Cleaning up the user's input (PS: Never trust the user )
    decimal_places = Number.parseInt(decimal_places); 
    numberValue = Number.parseFloat(numberValue); 

    // Execute this block of code below 
    let int_part = String(numberValue).split(".")[0]; 
    let floating_point = String(numberValue).split(".")[1]; 

    // Creating a list for holding the splitted number
    let listNumbers = [int_part.concat(".")]; 

    // 
    if ( decimal_places === 0 ) {
        // 
        return Number.parseInt(int_part); 
    }

    // 
    if ( decimal_places > floating_point.length ) {
        // 
        return numberValue; 
    }

    // looping and converting the numbers to the specified decimal places 
    for ( let i = 0; i < floating_point.length; i++ ) {
        // 
        if ( i === decimal_places - 1) {
            listNumbers.push(floating_point[i]); 

            // Return the final results 
            let res = Number.parseFloat(listNumbers.join("")); 
            return res 
        }

        // 
        listNumbers.push(floating_point[i]); 
    }
}

let results = convert_to_x_dp(1, number); 
console.log(results); 


