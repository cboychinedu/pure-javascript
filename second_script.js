/* Taking in the users input */ 
// Taking the users input
const name = prompt("What is your name? ");
const age = prompt("What is your age? ");
const email = prompt("What is your email? ");

// Saving the users input
// const userInput = [name, age, email];

// Saving the user's input inside a dictionary
const userInput = {
  "Name": name,
  "Age": age,
  "Email": email
};

// Displaying the user's input
alert("Your name is " + userInput["Name"]);
alert("You are " + userInput["Age"] + " old.");
alert("Your email is " + userInput["Email"]);



// // Converting numbers into strings
// var number = 10;
// var result = number.toString();
//
// // finding pi using mdn
// const PI = Math.PI;
// console.log(PI);
//
// // Finding the min values
// const min_value = Math.min(10, 20, 30, 40, 4);
// console.log(min_value);
//
// // Displaying the result
// console.log(result);
//
// // Displaying the type of the result
// console.log(typeof result);





// Using the prompt command
// var name = prompt("What is your name? ");
// console.log("Hello " + name + " ,how are you doing today? ");
//
// // Converting into an integer
// var age = prompt("What is your age? ");
// var newAge = parseInt(age);
//
// //
// console.log(typeof newAge);
