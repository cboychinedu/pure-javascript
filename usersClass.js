// Working with javascript classes 
// Creating a basic class type 
class UserCredentials {
    // Building the constructor 
    constructor(params) {
        this.firstname = params.firstname; 
        this.lastname = params.lastname; 
        this.age = params.age; 
        this.location = params.location; 
        this.country = params.country; 
        this.latitude = params.latitude; 
        this.longitude = params.longitude; 
    }

    // Creating a function for displaying the user first and last name 
    displayName = () => {
        // Execute this block of code if the display name function was called upon 
        console.log(`Your Firstname is: ${this.firstname}`); 
        console.log(`Your Lastname is: ${this.lastname}`); 
    }

    // Creating a function for displaying the users age and location 
    displayAgeAndLocation = () => {
        // Execute this block of code if this method was called upon with the created 
        // user class 
        console.log(`Your Age is: ${this.age}`); 
        console.log(`Your Location is: ${this.location}`); 
    }

    // Creating a function for displaying the users country, latitude, and longitude 
    displayCountryLatitudeAndLongitude = () => {
        // Execute this block of code if the fuction was called within the class 
        console.log(`Your country is: ${this.country}`); 
        console.log(`Your Latitude is: ${this.latitude}`); 
        console.log(`Your longitude is: ${this.longitude}`); 
    }
}

// Exporting the class 
module.exports.UserCredentials = UserCredentials; 

  

