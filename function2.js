/*
  Write a javascript function that accepts an array and an
  item, and checks if the item is in the array

*/
// Creating a function to check for the presence of an item in an array
// Which is parsed as an argument
function check_item(array, item)
{
    // Execute this block of code if the for-loop condition was satified
    // Checking to see if the values in the array equals the item-arg
    let COND = array.indexOf(item)

    // if/else condtion if the value for the item is present or not
    if ( COND !== -1 )
    {
        // The item is present in the array
        console.log(item + " is present in the array.");
    }

    // This condition is to check for the absence of a negative value
    else if ( COND === -1 )
    {
      // The item is not present in the array
      console.log(item + " is not present in the array.");
    }


}


// Creating an array and the item to be checked against
let new_arr = [20, 20, 13, 15, 19, 100];
let arr1 = [10, 20, 0.98, 100, 200, 43];
let arr2 = [20, 30, 100, 0.87, 0.33, 12.3];
let item_value = 10;

// Execution of the check_item function
check_item(new_arr, item_value);


/*
  Write a javascript function that accepts an arrays, and return all the
  arrays concatenated

*/
// Creating a function that accepts an arrays, and return all the arrays
// Concatenated
function concat_array1(arr1, arr2)
{
    // Concating the arrays
    let conc_values = arr1.concat(arr2);

    // Returning the result
    return conc_values;

}

// concat-arry2
function concat_array2()
{
    // Creating a list for holding the concatenated values
    let concat_array = [];

    // Creating a for-loop for exection
    for ( var i = 0; i < arguments.length; i++ )
    {
        //
        console.log(arguments[i]);
        console.log("The list number is: " + arguments.length);
        arguments[i].forEach(function(array)
      {
          // Adding the list items into the concat array
          concat_array.push(array);
      })

    };

    //
    console.log(concat_array);

};

// Execution of the concat_arry function
console.log(concat_array1(new_arr, arr2));
console.log(concat_array2([2, 3, 4, 10], [10, 40, 50, 0.887]));
console.log(return_smallest_value([200, 100, 20, 32, 0.899]));


/*
  Write a javascript function that takes in array and returns the smallest
  value
*/
// Creating the function that takes in an array an return the smallest value
function return_smallest_value(arr1)
{
    // Returning the smallest value
    let small_value;
    for ( var i = 0; i < arguments.length; i++ )
    {
        //
        console.log(arguments[i]);
    }

};


/*
  Write a javaScript function that accepts an array, and a value,
  and returns a new array with values then the values provided

*/
//
function greaterThan(arr, value)
{
    // Using the if/statement
    if (value)
    {
      // Using the filter method to extract the value
      var res = arr.filter(function(e) {
        // Returning the value
        return e > value;

      })

      // Returning the res array
      return res;
    }

    // else block statement
    else
    {
      // Returning the array
      return arr;

    }
}

//
console.log(greaterThan([5, 8, 6, 10, 20]));


// - Understanding closures
function counter()
{
    // Creating a varaible to hold the count value
    let count = 0;

    // Creating a returning a function for incrementing the value for count
    return function()
    {
        // Incrementing the value for i
        i++;
        console.log(i); 
    }
}

//
