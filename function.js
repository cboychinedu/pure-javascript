// Creating a function to take in two numbers
function rand(a, b)
{
  // Using an if/else condition
  if ( a === undefined && b === undefined )
  {
    // execute this block of code if the condition is met
    return 0;
  }

  // else if block condition
  else if ( b === undefined )
  {
    // execute this block of code if the condition is met
    return a;
  }
}

// Creating a varaible called summedvalues which will hold the sum result
// of the values from the "sum" function
let summedvalues = 0;

// Creating another function called sum, which would add all the
// numbers together
function sum(a, b)
{
  // Creating a for-loop to loop through the argument list
  for (var i = 0; i < arguments.length; i++ )
  {
    console.log(arguments[i]);
    summedvalues = summedvalues + arguments[i];

  }
}

// Creating a function-constructor
function MakePerson(name, email, age)
{
  this.name = name;
  this.email = email;
  this.age = age;
}

// Creating an object from the function makeperson
var person1 = new MakePerson("Hichem", "sd@sdd.fr", 23);
var person2 = new MakePerson("Mike", "sdd@fr.co.uk", 32);


// Creating a list/array that executes another function
let employee = {
  name: "Hichme",
  email: "sds@sd.fr",
  job: "Full Stack Developer",
  display: function(){
    // Executing this code if the function was called upon
    console.log("Name: " + this.name + "Email: " + this.email);

  }
}


sum(3, 5, 10, 20, 0.5);

console.log(person1);
console.log(person2);
console.log(rand(5));
console.log(employee["display"]);
