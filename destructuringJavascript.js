// Creating an element 
let person = {
    name: "Mbonu Chinedum", 
    sex: "Male", 
    age: 21, 
    location: "Awka, Anambra State."
}; 

// Using javascript ES6 Implementation "Destructing" 
const { name: new_name } = person; 

// --- OR 
// const { name } = person; 
// console.log(name); ==> Gives you "Mbonu Chinedum" 

// Displaying the output 
console.log(new_name); 