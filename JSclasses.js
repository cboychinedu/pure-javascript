// Creating a class called Person 
class Person{
    // Creating a constructor to get the values for "name" and "age" 
    constructor(name, age) {
        // Adding values to the constructor object 
        console.log("From the constructor."); 
        this.name = name; 
        this.age = age; 

    }

    // Adding a method to the "Person" class 
    displayName(babe) {
        // Creating a function to display the name of the person 
        // Passed from the constructor class 
        console.log(this.name);
        console.log(`The second parsed value is: ${babe}`);  
    }
}

// Creating an instance of the "Person" class 
let kelvinClass = new Person("Kelvin", 25); 
kelvinClass.displayName("Sarah"); 

// Exporting the class 
// module.exports = { Person }; 


// Creating a class called "Student" which inherits the "constructor" object from the 
// "Person" class 
// class Student extends Person{
//     constructor(name, age, department, level, university, location, faculty){
//         // Inheriting the values from the "Person" class 
//         super(name, age); 
//         this.department = department; 
//         this.level = level; 
//         this.university = university; 
//         this.locatiobn = location; 
//         this.faculty = faculty; 
//     }

//     // Creating a method for displaying the object inside the "Student" class 
//     displayObjects() {
//         console.log("The name is: " + this.name); 
//         console.log("University: " + this.university); 
//         console.log("Location: " + this.location); 
//         console.log("Faculty: " + this.faculty); 
//     }
// }

// Creating an instance of the "Person" created class 
// let kelvinClass = new Person("Kelvin", 25); 

// let new_student = new Student("Kelvin", 21, "Chemical Engineering", "300lv", "Nnamdi Azikiwe University", "Awka", "Engineering"); 
// new_student.displayObjects(); 


// Displaying the created class 
// kelvinClass.displayName("Sarah."); 