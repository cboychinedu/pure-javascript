// Using the setTimeout function to execute a block of 
// Code after some seconds 
// Setting the time delay before code execution 
const TIME = 1000; 
setTimeout(() =>
{
    // Execute this block of code after the specified time
    console.log("Hello User!"); 

}, TIME); 

