// Define a function 
const square = function(x) {
    return x * x; 
}; 

// func 1 
const power = function(base, exponent, mass) {
    let result = 1; 
    for (let count = 0; count < exponent; count++) {
        result *= base; 
    }

    // 
    let weight = function(gravity) {
        let res = mass * gravity; 
        return res; 

    }

    // 
    return result; 

}; 


let result = square(2); 
console.log(result); 
