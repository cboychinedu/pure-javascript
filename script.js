// Creating a variable to hold numbers
// var num = -10;
//
// console.log(num);
//
// var myName = "Hichem";
// console.log(myName);
//
// var isHere = true;
// console.log(isHere);

// Creating variables
var x = 3;
var y = 10;

// Perfroming some calculations
var addition = x + y;
var substraction = x - y;
var multiplication = x * y;
var division = x / y;
var modulus = x % y;


// Displaying the results
console.log(addition);
console.log(substraction);
console.log(multiplication);
console.log(division);
console.log(modulus); 
