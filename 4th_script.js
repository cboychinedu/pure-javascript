/* Guessing the right number */
// Creating the number
const guessed_number = 20;

// Generating a random number using the math function
const random_number = Math.random();

// Creating an array for the days of the week
var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var month = ["Jan", "Feb"];
console.log(weekday);

// Getting the date
const date = new Date();
console.log(date);

// Getting the day from the date
const day = date.getDay();
const new_date = date.getDate();
const fullYear = date.getFullYear();
const hours = date.getHours();
const months = date.getMonth();
const seconds = date.getSeconds();

//
console.log(weekday[day]);
console.log(new_date);
console.log(fullYear);
console.log(hours);
console.log(months);
console.log(seconds);

// Displaying the date information
var today_date = weekday[day] +  "," + month[months] + ", " + fullYear;
//
document.getElementById("date_section").innerHTML = today_date;
console.log(random_number);

// Taking the users input
var users_guess = prompt("Guess the number: ");

// Converting the user's number into an integer
user_guess = parseInt(users_guess);

// Checking if the user's input was the actual guessed number
// By using if/else statement
if ( user_guess === guessed_number )
{
  // Executing the block of code
  alert("You guessed the right number.");
}

// Else if statement
else if ( user_guess != guessed_number )
{
  // Executing the block of code is the condition was satified
  alert("You guessed the wrong number.");
}



// // Creating a variable to hold the age
// var age = prompt("How old are you? ");
//
// // Converting the age string into a integer
// age = parseInt(age);
//
// // Using if/else statement
// if ( age >= 18 )
// {
//   // Execute this code if the condition is satified
//   alert("Welcome to the website");
// }
//
// // Using the else if block
// else if ( age > 13 && age <= 18 )
// {
//   // Executing this block of code if the condition is met
//   alert("You're not allowed to access this webiste.");
// }
