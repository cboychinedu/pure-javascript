// --1 normal function 
function square(number) {
    number = number ** 2; 
    return number; 

}; 

// --2 function 
let squareIt = function(number) {
    number = number ** 2; 
    return number; 

}; 

// --2 function 
// using arrow function 
const power = (base, exponent) => {
    let result = 1; 

    // Using a for loop to increment the values for the base and 
    // assing them a new variable called "result" 
    for (let count = 0; count < exponent; count++) {
        result *= base; 
    }

    // Return the result 
    return result; 

}

// 
// console.log(power(20, 0.87)); 
const result = squareIt(5); 
console.log(result); 