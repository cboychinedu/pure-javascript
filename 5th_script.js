/* Using for-loop in javaScript */
// The first loop to loop for 9 iterations
// for ( var i = 0; i < 10; i++ )
// {
//   // Execute this block of code
//   console.log("Hello World!");
//
// }
//
// var count = true;
//
// // Using the do-while loop
// do {
//   console.log("Still counting.");
// }
// while ( count !== false )

// Creating an array
var carsIlike = ["BMW", "Toyota", "kia"];
document.getElementById("greetings").style.color = "blue";

/* Creating a function */
function function_name(number)
{
  // Code-block for the function
  console.log(number);
};

// Creating the first function
function x_function(condition, num)
{
  // execute this block of code if this function was called
  console.log("This function was called (x_function)");
  console.log(condition);

  // Checking
  let color_value = document.getElementById(condition).style.color;
  let background_color = document.getElementById(condition).style.backgroundColor;

  //
  document.getElementById("pressed_btn").innerHTML = "You pressed: " + num;

  // if/else block
  if ( color_value === "" && background_color === "" || color_value === "black" || background_color === "white" )
  {
    // Changing the color for the button
    document.getElementById(condition).style.color = "red";
    document.getElementById(condition).style.backgroundColor = "black";
  }

  // else/if block
  else if ( color_value === "white" || background_color === "black" )
  {
    // execute this block of code if the condition above is satified
    document.getElementById(condition).style.color = "black";
    document.getElementById(condition).style.backgroundColor = "white";
  }


}
