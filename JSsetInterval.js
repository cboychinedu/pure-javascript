// Using the setInterval function 
// Setting the time delay 
const TIME = 5000; 
let execution = setInterval(() =>
{
    // Execute this block of code at the specific 
    // declared time interval
    console.log("Executing at 3seconds intervals"); 

}); 

// Stopping the code after some seconds interval 
clearInterval(execution); 