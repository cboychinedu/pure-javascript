/* Using if/else statement */
// Creating a list to hold some specific names
const names = ["Chinedu", "Mike", "Sarah"];

// Asking the user for a specific color
var color_value = prompt("Which color: ");
color_value = color_value.toString();

// if the user input is red, then turn the paragraph to the color red
if ( color_value === "red" )
{
  // Change the color of the background to red
  document.getElementById("greetings").style.color = "red";
}

// if the user input is blue, then turn the paragraph to the color blue
else if ( color_value ===  "blue" )
{
  // Change the color of the background to blue
  document.getElementById("greetings").style.color = "blue";
}

// if the color is yellow
else if ( color_value === "yellow")
{
  // Execute this block of code if the condition is satified
  document.getElementById("greetings").style.color = "yellow"; 
}

// Taking the users input
// var users_input = prompt("What is your name: ");

// Converting the users input into a string
// users_input = users_input.toString();

// Using an if/else statement
// if ( users_input === names[1] )
// {
//   // Execute this block of code if the condition above is satified.
//   console.log("Hello " + names[1]);
// }
//
// // Using the else statement
// else
// {
//   // Execute this block of code if the condition is not satified
//   console.log("Hello " + users_input + " sorry, your name is not Mike.");
// }
//
// console.log(typeof users_input);
