// Run bash script using Nodejs/JavaScript 
// Importing the necessary modules 
const exec = require('child_process').exec; 

// Executing the bash file using the "exec" command and getting 
// Back the stdout value 
const shellScript = exec('bash shell_script.sh'); 

// Getting the result for the execution 
shellScript.stdout.on('data', (data) =>
{
    // Getting the output for the execution of the shell script "shell_script.sh" 
    console.log(data); 

}); 

