// Run shell script using javaScript 
// Importing the necessary modules 
const { exec } = require('child_process'); 

// Executing the shell command 
exec("bash shell_script.sh", (error, stdout, stderror) =>
{
    // Checking for errors 
    if (error) 
    {
        // On error for execution, exeute this block of code. 
        console.log(error); 
    }

    // On successful execution 
    else 
    {
        // Execute this block of code if the execution was successful 
        console.log(stdout); 
    }
}); 

