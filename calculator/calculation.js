/* This script is for the buttons pressed */
// Creating an array for holding each individual button and
// its respective values
let button_values = {
  "addition": "+",
  "division": "/",
  "multiplication": "*",
  "substraction": "-",
  "dot": ".",
  "zero": 0,
  "one": 1,
  "two": 2,
  "three": 3,
  "four": 4,
  "five": 5,
  "six": 6,
  "seven": 7,
  "eight": 8,
  "nine": 9
}

// Creating an array to hold the values for pressure
let pressure_values = [];

// Creating an array to hold the values for volume
let volume_values = [];

// Creating a variable to hold the cursor location value
let cursor_location;

// Cursor -properties
let cursor_information = {
  volume_id: "cal_div_screen_volume_para",
  pressure_id: "cal_div_screen_pressure_para"
}

// Creating a function for clearing the screen
function clear_screen()
{
    // CODE EXECUTION
    // Clearing the constant result
    document.getElementById("cal_div_screen_const_para").innerHTML = "0.0";

    // Clearing the volume result
    document.getElementById("cal_div_screen_volume_para").value = "0.0";
    volume_values = [];

    // Clearing the pressure result
    document.getElementById("cal_div_screen_pressure_para").value = "0.0";
    pressure_values = [];
}


/* Creating a function to check where the selection is using the mouse, or
* we just need a function to know where the cursor is and return the id-selector value */
function cursor_location_function(location)
{
    /* This function would look for the cursor location and return the Id value for where
    * it is located */
    // Code Execution
    if ( location === "volume_id")
    {
        // Getting the cursor information id-value for volume from the cursor_information dictionary
        cursor_location = cursor_information[location];

    }

    // If the condition above is not satified, then get the id-value for the pressure
    else if ( location === "pressure_id" )
    {
        // Getting the cursor information id-value for pressure from the cursor_information dictionary
        cursor_location = cursor_information[location];

    }
}

// Creating a function for the button call
function number_button(button_name)
{
    // Execute this block of code if the button is pressed
    // Creating a variable to hold the result for the button name
    let result = button_values[button_name];

    /***** Getting the cursor location  *****/


    // IF/ELSE statement
    let CONDITION = cursor_location.split("_")[3];

    // Checking if the cursor is at the volume screen
    if ( CONDITION === "volume" )
    {
        // Execute this block if the condition is met
        // Saving the values pressed into the list
        document.getElementById(cursor_location).value = result;
        volume_values.push(result);

        //
        let input_values = volume_values.join("").toString();
        input_values = parseInt(input_values);
        document.getElementById(cursor_location).value = input_values;

    }

    // Checking if the cursor is at the pressure screen
    else if ( CONDITION === "pressure" )
    {
        // Execute this block of the condition is met
        // Saving the values pressed into the list
        document.getElementById(cursor_location).value = result;
        pressure_values.push(result);

        //
        let input_values = pressure_values.join("").toString();
        input_values = parseInt(input_values);
        document.getElementById(cursor_location).value = input_values;

    }


    // DEBUG
    console.log("button pressed");
    console.log(button_name);
}


/** Creating a function for calculating everything **/
// Creating a function for calculating the constant
function calculateAll()
{
    // CODE EXECUTION
    // Checking if both arrays for the volume and pressure is occupied
    let input_pressure = document.getElementById("cal_div_screen_pressure_para").value;
    input_pressure = parseFloat(input_pressure);

    // Getting the values for the volume
    let input_volume = document.getElementById("cal_div_screen_volume_para").value;
    input_volume = parseFloat(input_volume);

    // Getting the constant
    let constantValue = input_pressure * input_volume;

    // Checking for NaN value
    if ( constantValue.toString() === "NaN" )
    {
        // code execution if the value for the constant is not a number
        document.getElementById("cal_div_screen_const_para").innerText = "0.0";
    }

    //
    else
    {
        // code execution if the value for the constant is a number
        document.getElementById("cal_div_screen_const_para").innerText = constantValue;
    }

    // DUBUG
    console.log(input_pressure);
    console.log(input_volume);

}
