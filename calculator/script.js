// -Checking the script
console.log("Working");

// Setting the current screen display value
let screen_display = 0;

// FIRST VALUES
let first_input = [];
let first_input_values;

// section value
let section_value = "first";
// let operand_btn_used = [];
let operand_btn_used;

// Setting the screen display from the default value
document.getElementById("cal_div_screen_volume_para").innerText = screen_display;

// Creating an array for holding each individual button and
// its respective values
let button_values = {
  "addition": "+",
  "division": "/",
  "multiplication": "*",
  "substraction": "-",
  "dot": ".",
  "zero": 0,
  "one": 1,
  "two": 2,
  "three": 3,
  "four": 4,
  "five": 5,
  "six": 6,
  "seven": 7,
  "eight": 8,
  "nine": 9

}

// Creating a function for calculations
function calculate_inputs()
{
    //**** Code execution statement for the first input values  ***//
    first_input_values = first_input.join('').toString();

    // Converting the first input values into integers
    first_input_values = parseInt(first_input_values);

}


// Creating a function for each button pressed
function button_call(button_name)
{

    // Working on the operand button used
    if ( button_name === "division" || button_name === "substraction" || button_name === "addition" ||
         button_name === "multiplication" )
    {
      // Execute this code if the operand button_name is satified and is being used
      // operand_btn_used.push(button_name);
      operand_btn_used = button_name;
    }

    // Execute this block of code only before a symbol is pressed and
    // Keep on adding and displaying the numbers for the first section on the screen.
    if ( section_value === "first" )
    {
        // Code execution statement
        let result = button_values[button_name];
        document.getElementById("cal_div_screen_volume_para").innerText = result;

        // Saving the pressed button value into the list
        first_input.push(result);

        // // DEBUG
        console.log(result);
        console.log(first_input.join());
        calculate_inputs();

        //
        document.getElementById("cal_div_screen_volume_para").innerText = first_input_values;

    }

};


// Creating a function for calculation
function calculateAll()
{

    // The function is responsible for calculation of the whole numbers
    let splitedArray = first_input.join("").split(button_values[operand_btn_used])     // <<== Take note, the division symbol was used here.

    // Checking the operator for the operand button used
    if ( operand_btn_used === "division")
    {
      // Performing divsison on the numbers
      let divided_answer = splitedArray[0] / splitedArray[1];

      // Displaying the results
      document.getElementById("cal_div_screen").innerText = divided_answer;
      console.log(splitedArray[0] / splitedArray[1]);
    }
}
