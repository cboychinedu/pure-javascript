// Mapping in javascript
const numbers = [4, 9, 16, 25];
let myFunction = (num) => {
  let result = num * 10;
  return result;
}
const newArr = numbers.map(myFunction)


// Loop through each values in the numbers array, and
// add one to each looped numbers, and return the result.
const secondArr = numbers.map((numbers) => {
    //
    addedRes = numbers + 1;
    return addedRes;
});

console.log(secondArr);


// -- 3
//
const persons = [
    { firstname: "Malcom", lastname: "Reynolds" },
    { firstname: "Kaylee", lastname: "Frye" },
    { firstname: "Jayne", lastname: "Cobb" }
];

// Creating a function for mapping the firstname and lastname together
const result = persons.map((data, index) => {
    //
    data = `${index} ${data.firstname} ${data.lastname}`;
    return data;
});

console.log(result);
